from conans import ConanFile, CMake, tools

class HelloWorldConan(ConanFile):
    name = "hello_world_exec"
    version = "0.1"
    description = "Executable with hello world"
    topics = ("hello world")
    url = "https://gitlab.com/conan-helloworld/hello-world-exec"
    license = "MIT"

    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def requirements(self):
        self.requires("hello_world_lib/0.1@conan-helloworld+hello-world-library/stable")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
