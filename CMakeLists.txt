cmake_minimum_required(VERSION 3.12)
set(CMAKE_CXX_STANDARD 17)

# conan package manager
if(CONAN_EXPORTED)
    ## conan install already called
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup()
else()
    if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
        message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
        file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.16/conan.cmake"
            "${CMAKE_BINARY_DIR}/conan.cmake"
            TLS_VERIFY ON)
    endif()

    include(${CMAKE_BINARY_DIR}/conan.cmake)

    conan_cmake_run(CONANFILE conanfile.py BUILD_TYPE ${CMAKE_BUILD_TYPE} BUILD missing
                    BASIC_SETUP KEEP_RPATHS)
endif()

file(GLOB SOURCES "src/*.cpp" "src/*.h")
add_executable(hello_world ${SOURCES})
target_include_directories(hello_world PUBLIC ${CONAN_INCLUDE_DIRS})
target_link_libraries(hello_world ${CONAN_LIBS})
